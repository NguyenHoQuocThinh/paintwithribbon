// *****************************************************************************
// * This is an automatically generated header file for UI Element definition  *
// * resource symbols and values. Please do not modify manually.               *
// *****************************************************************************

#pragma once

#define cmdTabHome 2 
#define cmdTabHome_LabelTitle_RESID 60001
#define cmdTabTool 3 
#define cmdTabTool_LabelTitle_RESID 60002
#define cmdGroupFile 4 
#define cmdGroupFile_LabelTitle_RESID 60003
#define cmdGroupDraw 5 
#define cmdGroupDraw_LabelTitle_RESID 60004
#define ID_BTN_NEW 6 
#define ID_BTN_NEW_LabelTitle_RESID 60005
#define ID_BTN_OPEN 7 
#define ID_BTN_OPEN_LabelTitle_RESID 60006
#define ID_BTN_SAVE 8 
#define ID_BTN_SAVE_LabelTitle_RESID 60007
#define ID_BTN_EXIT 9 
#define ID_BTN_EXIT_LabelTitle_RESID 60008
#define ID_BTN_LINE 10 
#define ID_BTN_LINE_LabelTitle_RESID 60009
#define ID_BTN_RECTANGLE 11 
#define ID_BTN_RECTANGLE_LabelTitle_RESID 60010
#define ID_BTN_ELLIPSE 12 
#define ID_BTN_ELLIPSE_LabelTitle_RESID 60011
#define InternalCmd2_LabelTitle_RESID 60012
