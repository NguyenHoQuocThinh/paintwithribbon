#include "stdafx.h"
#include "Square.h"


CSquare::CSquare() {
	type = 3;
}


CSquare::CSquare(int intype, int a, int b, int c, int d)
{
	type = intype;
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CSquare::~CSquare()
{
}


CShape* CSquare::Create(int type, int a, int b, int c, int d) {
	return new CSquare(type, a, b, c, d);
}

int CSquare::GetType() {
	return this->type;
}

int CSquare::GetLeft() {
	return this->left;
}

int CSquare::GetTop() {
	return this->top;
}

int CSquare::GetRight() {
	return this->right;
}

int CSquare::GetBottom() {
	return this->bottom;
}


void CSquare::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, left, top, right, bottom);
}

