#include "stdafx.h"
#include "Line.h"

CLine::CLine() {
	type = 0;
}

CLine::CLine(int intype, int a, int b, int c, int d)
{
	type = intype;
	beginX = a;
	beginY = b;
	endX = c;
	endY = d;
}

CLine::~CLine()
{
}

CShape* CLine::Create(int type, int a, int b, int c, int d) {
	return new CLine(type, a, b, c, d);
}

int CLine::GetType() {
	return this->type;
}

int CLine::GetLeft() {
	return this->beginX;
}

int CLine::GetTop() {
	return this->beginY;
}

int CLine::GetRight() {
	return this->endX;
}

int CLine::GetBottom() {
	return this->endY;
}

void CLine::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	MoveToEx(hdc, beginX, beginY, NULL);
	LineTo(hdc, endX, endY);
}
