#include "stdafx.h"
#include "Ellipse.h"


CEllipse::CEllipse() {
	type = 2;
}


CEllipse::CEllipse(int intype, int a, int b, int c, int d)
{
	type = intype;
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CEllipse::~CEllipse()
{
}


CShape* CEllipse::Create(int type, int a, int b, int c, int d) {
	return new CEllipse(type, a, b, c, d);
}

int CEllipse::GetType() {
	return this->type;
}

int CEllipse::GetLeft() {
	return this->left;
}

int CEllipse::GetTop() {
	return this->top;
}

int CEllipse::GetRight() {
	return this->right;
}

int CEllipse::GetBottom() {
	return this->bottom;
}


void CEllipse::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Ellipse(hdc, left, top, right, bottom);
}
