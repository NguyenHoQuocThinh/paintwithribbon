#include "stdafx.h"
#include "Rectangle.h"


CRectangle::CRectangle() {
	type = 1;
}


CRectangle::CRectangle(int intype, int a, int b, int c, int d)
{
	type = intype;
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CRectangle::~CRectangle()
{
}

CShape* CRectangle::Create(int type, int a, int b, int c, int d) {
	return new CRectangle(type, a, b, c, d);
}

int CRectangle::GetType() {
	return this->type;
}

int CRectangle::GetLeft() {
	return this->left;
}

int CRectangle::GetTop() {
	return this->top;
}

int CRectangle::GetRight() {
	return this->right;
}

int CRectangle::GetBottom() {
	return this->bottom;
}

void CRectangle::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, left, top, right, bottom);
}