﻿// MyPaint1.0.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "MyPaint1.0.h"
#include "Shape.h"
#include "Rectangle.h"
#include "Square.h"
#include "Line.h"
#include "Ellipse.h"
#include "Circle.h"
#include <vector>
#include <fstream>
#include <iostream>
#include <windows.h>
#include <commctrl.h>
#pragma comment(lib, "comctl32.lib")
using namespace std;

#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

#include <Objbase.h>
#pragma comment(lib, "Ole32.lib")
#include "RibbonFramework.h"
#include "RibbonIDs.h"



#include <windowsx.h>
#define MAX_LOADSTRING 100

#define DRAW_LINE 0;
#define DRAW_RECTANGLE 1;
#define DRAW_ELLIPSE 2;
#define DRAW_SQUARE 3;
#define DRAW_CIRCLE 4;

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

void setRectangle(Rect &r, int left, int top, int right, int bottom);
void SaveFile();
void OpenFile();
CShape* SaveShape(int startX, int startY, int lastX, int lastY);

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	HRESULT hr = CoInitialize(NULL);
	if (FAILED(hr)) {
		return FALSE;
	}
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_MYPAINT10, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MYPAINT10));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
	CoUninitialize();
    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = 0;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_MYPAINT10));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_MYPAINT10);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, L"My Paint 2.0", WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 1600, 900, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
bool isDrawing = false;
bool isShift = false;
int startX;
int startY;
int lastX;
int lastY;

HDC          hdcOld;
HBITMAP      hbmOld;

HDC          hdcStart;
HBITMAP      hbmStart;

int icurrent = 0;
HWND statusBar;
GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;

vector<CShape*> shapes;
vector<CShape*> prototype;

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	bool initSuccess;
    switch (message)
    {
	case WM_CREATE: {
		initSuccess = InitializeFramework(hWnd);
		if (!initSuccess) {
			return -1;
		}
		GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
		
		prototype.push_back(new CLine());
		prototype.push_back(new CRectangle());
		prototype.push_back(new CEllipse());
		prototype.push_back(new CSquare());
		prototype.push_back(new CCircle());
		
		
		statusBar = CreateWindow(STATUSCLASSNAME, NULL, WS_CHILD | WS_VISIBLE | SBARS_SIZEGRIP, 0, 0, 0, 0, hWnd, NULL, hInst, NULL);
		TCHAR *buffer = new TCHAR[50];
		wsprintf(buffer, L"");
		SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
	}break;
	case WM_MOUSEMOVE: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		if (isDrawing) {
			lastX = x;
			lastY = y;
			InvalidateRect(hWnd, NULL, FALSE);
		}

		if (hdcStart == NULL) {
			HDC hdc = GetDC(hWnd);
			RECT Client_Rect;
			GetWindowRect(hWnd, &Client_Rect);
			int win_width = Client_Rect.right - Client_Rect.left;
			int win_height = Client_Rect.bottom - Client_Rect.top;
			hdcStart = CreateCompatibleDC(hdc);
			hbmStart = CreateCompatibleBitmap(hdc, win_width, win_height);
			SelectObject(hdcStart, hbmStart);
			BitBlt(hdcStart, 0, 0, win_width, win_height, hdc, 0, 0, SRCCOPY);
			ReleaseDC(hWnd, hdc);
		}
	}break;
	case WM_LBUTTONDOWN: {
		int x = GET_X_LPARAM(lParam);
		int y = GET_Y_LPARAM(lParam);
		startX = x;
		startY = y;
		isDrawing = true;
		HDC hdc = GetDC(hWnd);
		RECT Client_Rect;
		GetWindowRect(hWnd, &Client_Rect);
		int win_width = Client_Rect.right - Client_Rect.left;
		int win_height = Client_Rect.bottom - Client_Rect.top;
		hdcOld = CreateCompatibleDC(hdc);
		hbmOld = CreateCompatibleBitmap(hdc, win_width, win_height);
		SelectObject(hdcOld, hbmOld);
		BitBlt(hdcOld, 0, 0, win_width, win_height, hdc, 0, 0, SRCCOPY);
		ReleaseDC(hWnd, hdc);

	}break;
	case WM_LBUTTONUP: {
		CShape* shape = SaveShape(startX, startY, lastX, lastY);
		shapes.push_back(shape);
		isDrawing = false;
		InvalidateRect(hWnd, NULL, FALSE);
		DeleteObject(hbmOld);
		DeleteDC(hdcOld);
	}break;
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
			{
			case ID_BTN_OPEN: {
				OpenFile();
				HDC hdc = GetDC(hWnd);
				for (int i = 0; i < shapes.size(); i++) {
					shapes[i]->Draw(hdc);
				}
				ReleaseDC(hWnd, hdc);
				InvalidateRect(hWnd, NULL, FALSE);
				break;
			}
			case ID_BTN_SAVE: {
				SaveFile();
				break;
			}
			case ID_BTN_NEW: {
				HDC hdc = GetDC(hWnd);
				RECT Client_Rect;
				GetWindowRect(hWnd, &Client_Rect);
				int win_width = Client_Rect.right - Client_Rect.left;
				int win_height = Client_Rect.bottom - Client_Rect.top;
				BitBlt(hdc, 0, 0, win_width, win_height, hdcStart, 0, 0, SRCCOPY);
				ReleaseDC(hWnd, hdc);
				InvalidateRect(hWnd, NULL, FALSE);
				break;
			}
            case ID_BTN_EXIT:
                DestroyWindow(hWnd);
                break;
			case ID_BTN_LINE: {
				icurrent = 0;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Vẽ đường thẳng");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
				break;
			}
			case ID_BTN_RECTANGLE: {
				icurrent = 1;
				HMENU hmenu = GetMenu(hWnd);
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Vẽ hình chữ nhật");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
				break;
			}
			case ID_BTN_ELLIPSE: {
				icurrent = 2;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Vẽ hình ê-líp");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
				break;
			}
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
	case WM_SIZE:
	{
		RECT main;
		GetWindowRect(hWnd, &main);
		int nStatusSize[] = { main.right };
		SendMessage(statusBar, SB_SETPARTS, 1, (LPARAM)&nStatusSize);
		MoveWindow(statusBar, 0, 0, main.right, main.bottom, TRUE);
	}break;

    case WM_PAINT:
        {
			
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
			if (isDrawing) {
				RECT Client_Rect;
				GetWindowRect(hWnd, &Client_Rect);
				int win_width = Client_Rect.right - Client_Rect.left;
				int win_height = Client_Rect.bottom - Client_Rect.top;

				HDC hdcMem = CreateCompatibleDC(hdc);
				HBITMAP hbmMem = CreateCompatibleBitmap(hdc, win_width, win_height);
				SelectObject(hdcMem, hbmMem);

				BitBlt(hdcMem, 0, 0, win_width, win_height, hdcOld, 0, 0, SRCCOPY);


				Graphics* graphics = new Graphics(hdcMem);
				Pen* pen = new Pen(Color(255, 0, 0, 0), 1);
				if(icurrent == 0)
					graphics->DrawLine(pen, startX, startY, lastX, lastY);
				if (icurrent == 1) {
					Rect r;
					setRectangle(r, startX, startY, lastX, lastY);
					graphics->DrawRectangle(pen, r);
				}
				if (icurrent == 2)
					graphics->DrawEllipse(pen, startX, startY, lastX - startX, lastY - startY);
				if (icurrent == 3) {
					Rect r;
					setRectangle(r, startX, startY, lastX, lastY);
					graphics->DrawRectangle(pen, r);
				}
				if (icurrent == 4) {
					Rect r;
					setRectangle(r, startX, startY, lastX, lastY);
					graphics->DrawEllipse(pen, r);
				}
				BitBlt(hdc, 0, 0, win_width, win_height, hdcMem, 0, 0, SRCCOPY);
				//SelectObject(hdcMem, hbmMem);
				DeleteObject(hbmMem);
				DeleteDC(hdcMem);
			}
			
            EndPaint(hWnd, &ps);
        }
        break;
	case WM_KEYDOWN: {
		if (wParam == VK_SHIFT) {
			if (icurrent == 1) {
				icurrent = DRAW_SQUARE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Vẽ hình hình vuông");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			if (icurrent == 2) {
				icurrent = DRAW_CIRCLE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Vẽ hình hình tròn");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			isShift = true;
		}
	}break;
	case WM_KEYUP: {
		if (wParam == VK_SHIFT) {
			if (icurrent == 3) {
				icurrent = DRAW_RECTANGLE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Vẽ hình hình chữ nhật");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			if (icurrent == 4) {
				icurrent = DRAW_ELLIPSE;
				TCHAR *buffer = new TCHAR[50];
				wsprintf(buffer, L"Vẽ hình hình ê-líp");
				SendMessage(statusBar, SB_SETTEXT, 0, (LPARAM)buffer);
			}
			isShift = false;
		}

	}break;
    case WM_DESTROY:
		DestroyFramework();
		GdiplusShutdown(gdiplusToken);
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

void setRectangle(Rect &r, int left, int top, int right, int bottom) {
	if (icurrent == 1) {
		int width = abs(right - left);
		int height = abs(bottom - top);
		if (left<right&&top>bottom) {
			r.X = left;
			r.Y = top - height;
			r.Width = width;
			r.Height = height;
		}
		if (left < right&&top < bottom) {
			r.X = left;
			r.Y = top;
			r.Width = width;
			r.Height = height;
		}
		if (left > right&&top > bottom) {
			r.X = left - width;
			r.Y = top - height;
			r.Width = width;
			r.Height = height;
		}
		if (left > right&&top < bottom) {
			r.X = left - width;
			r.Y = top;
			r.Width = width;
			r.Height = height;
		}
	}
	if (icurrent == 3) {
		int side = abs(right - left);
		if (left<right&&top>bottom) {
			r.X = left;
			r.Y = top - side;
			r.Width = side;
			r.Height = side;
		}
		if (left < right&&top < bottom) {
			r.X = left;
			r.Y = top;
			r.Width = side;
			r.Height = side;
		}
		if (left > right&&top > bottom) {
			r.X = left - side;
			r.Y = top - side;
			r.Width = side;
			r.Height = side;
		}
		if (left > right&&top < bottom) {
			r.X = left - side;
			r.Y = top;
			r.Width = side;
			r.Height = side;
		}
	}
	if (icurrent == 4) {
		int side = abs(right - left);
		if (left<right&&top>bottom) {
			r.X = left;
			r.Y = top - side;
			r.Width = side;
			r.Height = side;
		}
		if (left < right&&top < bottom) {
			r.X = left;
			r.Y = top;
			r.Width = side;
			r.Height = side;
		}
		if (left > right&&top > bottom) {
			r.X = left - side;
			r.Y = top - side;
			r.Width = side;
			r.Height = side;
		}
		if (left > right&&top < bottom) {
			r.X = left - side;
			r.Y = top;
			r.Width = side;
			r.Height = side;
		}
	}
}

CShape* SaveShape(int startX, int startY, int lastX, int lastY) {
	if (isShift == false) {
		CShape* shape = prototype[icurrent]->Create(icurrent, startX, startY, lastX, lastY);
		return shape;
	}
	else {
		if (startX < lastX&&startY > lastY) {
			CShape* shape = prototype[icurrent]->Create(icurrent, startX, startY, lastX - 5, startY - (lastX - startX - 5));
			return shape;
		}
		if (startX < lastX&&startY < lastY) {
			CShape* shape = prototype[icurrent]->Create(icurrent, startX, startY, lastX - 5, startY + (lastX - startX - 5));
			return shape;
		}
		if (startX > lastX&&startY > lastY) {
			CShape* shape = prototype[icurrent]->Create(icurrent, startX - 5, startY, lastX, startY - (startX - lastX - 5));
			return shape;
		}
		if (startX > lastX&&startY < lastY) {
			CShape* shape = prototype[icurrent]->Create(icurrent, startX - 5, startY, lastX, startY + (startX - lastX - 5));
			return shape;
		}
	}

}


void SaveFile() {
	ofstream f;
	f.open("PaintSave.dat", ios::binary);
	for (int i = 0; i < shapes.size(); i++) {
		int type = shapes[i]->GetType();
		int left = shapes[i]->GetLeft();
		int top = shapes[i]->GetTop();
		int right = shapes[i]->GetRight();
		int bottom = shapes[i]->GetBottom();


		f.write((const char*)&type, sizeof(int));
		f.write((const char*)&left, sizeof(int));
		f.write((const char*)&top, sizeof(int));
		f.write((const char*)&right, sizeof(int));
		f.write((const char*)&bottom, sizeof(int));
	}
	f.close();
}

void OpenFile() {
	ifstream f;
	int type;
	int x[4];
	f.open("PaintSave.dat", ios::binary);
	while (!f.eof()) {
		f.read((char*)&type, sizeof(int));
		for (int i = 0; i < 4; i++) {
			f.read((char*)&x[i], sizeof(int));
		}
		if (type == 0) {
			CShape* DrawLine = prototype[type]->Create(type, x[0], x[1], x[2], x[3]);
			shapes.push_back(DrawLine);
		}
		if (type == 1) {
			CShape* DrawRectangle = prototype[type]->Create(type, x[0], x[1], x[2], x[3]);
			shapes.push_back(DrawRectangle);
		}
		if (type == 2) {
			CShape* DrawEllipse = prototype[type]->Create(type, x[0], x[1], x[2], x[3]);
			shapes.push_back(DrawEllipse);
		}
		if (type == 3) {
			CShape* DrawSquare = prototype[type]->Create(type, x[0], x[1], x[2], x[3]);
			shapes.push_back(DrawSquare);
		}
		if (type == 4) {
			CShape* DrawCircle = prototype[type]->Create(type, x[0], x[1], x[2], x[3]);
			shapes.push_back(DrawCircle);
		}
	}
	f.close();
}