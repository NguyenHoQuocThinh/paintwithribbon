#pragma once
#include "stdafx.h"
#include "Shape.h"
class CSquare: public CShape
{
private:
	int type;
	int left;
	int right;
	int top;
	int bottom;
public:
	CSquare();
	CSquare(int type, int a, int b, int c, int d);
	CShape* Create(int type, int a, int b, int c, int d);
	int GetType();
	int GetLeft();
	int GetTop();
	int GetRight();
	int GetBottom();
	void Draw(HDC hdc);
	~CSquare();
};

