#pragma once
#include "stdafx.h"

#include <objidl.h>
#include <gdiplus.h>
#pragma comment (lib,"Gdiplus.lib")
using namespace Gdiplus;

class CShape
{
public:
	CShape();
	virtual int GetType() = 0;
	virtual int GetLeft() = 0;
	virtual int GetTop() = 0;
	virtual int GetRight() = 0;
	virtual int GetBottom() = 0;
	virtual CShape *Create(int type, int a, int b, int c, int d) = 0;
	virtual void Draw(HDC hdc) = 0;
	~CShape();
};

