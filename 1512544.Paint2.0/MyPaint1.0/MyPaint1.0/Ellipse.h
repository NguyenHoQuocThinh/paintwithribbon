#pragma once
#include "stdafx.h"
#include "Shape.h"

class CEllipse: public CShape
{
private:
	int type;
	int left;
	int top;
	int right;
	int bottom;
public:
	CEllipse();
	CEllipse(int type, int a, int b, int c, int d);
	CShape* Create(int type, int a, int b, int c, int d);
	int GetType();
	int GetLeft();
	int GetTop();
	int GetRight();
	int GetBottom();
	void Draw(HDC hdc);
	~CEllipse();
};

