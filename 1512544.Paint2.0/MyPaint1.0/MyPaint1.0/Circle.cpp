#include "stdafx.h"
#include "Circle.h"


CCircle::CCircle() {
	type = 4;
}


CCircle::CCircle(int intype, int a, int b, int c, int d)
{
	type = intype;
	left = a;
	top = b;
	right = c;
	bottom = d;
}


CCircle::~CCircle()
{
}


CShape* CCircle::Create(int type, int a, int b, int c, int d) {
	return new CCircle(type, a, b, c, d);
}

int CCircle::GetType() {
	return this->type;
}

int CCircle::GetLeft() {
	return this->left;
}

int CCircle::GetTop() {
	return this->top;
}

int CCircle::GetRight() {
	return this->right;
}

int CCircle::GetBottom() {
	return this->bottom;
}


void CCircle::Draw(HDC hdc) {
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Ellipse(hdc, left, top, right, bottom);
}