#pragma once
#include "stdafx.h"
#include "Shape.h"


class CLine: public CShape
{
private:
	int type;
	int beginX;
	int beginY;
	int endX;
	int endY;
public:
	CLine(int type, int a, int b, int c, int d);
	CShape *Create(int type, int a, int b, int c, int d);
	int GetType();
	int GetLeft();
	int GetTop();
	int GetRight();
	int GetBottom();
	
	void Draw(HDC hdc);
	CLine();
	~CLine();
};

