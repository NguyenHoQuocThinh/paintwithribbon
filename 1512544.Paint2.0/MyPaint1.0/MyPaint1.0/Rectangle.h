#pragma once
#include "stdafx.h"
#include "Shape.h"

class CRectangle: public CShape
{
private:
	int type;
	int left;
	int right;
	int top;
	int bottom;
public:
	CRectangle();
	CRectangle(int type, int a, int b, int c, int d);
	CShape* Create(int type, int a, int b, int c, int d);
	int GetType();
	int GetLeft();
	int GetTop();
	int GetRight();
	int GetBottom();
	void Draw(HDC hdc);
	~CRectangle();
};

