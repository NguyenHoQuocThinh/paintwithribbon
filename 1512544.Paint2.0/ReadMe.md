﻿# Đại học khoa học tự nhiên
# -------------------------------------------------
###Họ tên: NGUYỄN HỒ QUỐC THỊNH
###MSSV 1512544
# -------------------------------------------------
#Chức năng đã hoàn thành:
1. Thao tác được chuột và bàn phím.
2. Vẽ được đường thẳng, hình chữ nhật, hình elip.
3. Nhấn giữ phím SHIFT thì hình chữ nhật sẽ là hình vuông, hình elip sẽ là hình tròn.
4. Loại hình đang được chọn được thể hiện ở thanh status
5. Thêm giao diện ribbon, thêm các tab, group, button trên ribbon
6. Lưu và mở file 
#Luồng sự kiện chính:
* Người dùng chọn loại hình ở bảng chọn MENU và vẽ, ấn giữ phím shift để vẽ hình đặc biệt
#Luồng sự kiện phụ:
*
#Link bitbucket: https://NguyenHoQuocThinh@bitbucket.org/NguyenHoQuocThinh/paintwithribbon
#Link video demo hướng dẫn: https://youtu.be/LXnxhuTKnmE

